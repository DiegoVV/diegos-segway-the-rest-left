# -*- coding: utf-8 -*-
import numpy
from .state import State
from .genetic import Genetic
import random
import datetime, time
from operator import itemgetter

genetic = Genetic()
MAX_EP = 100
next_iter = 50

class Controller:
    def __init__(self, game, load, state):
        self.initialize_parameters(game, load, state)

    def initialize_parameters(self, game, load,state):
        self.state = state
        self.pop = []
        if load == None:
            self.parameters = [random.randint(0,90),random.randint(0,100),random.randint(90,180)]
            #self.parameters = numpy.random.normal(0, 1, 3*len(self.compute_features()))
        else:
            params = open(load, 'r')
            weights = params.read().split("\n")
            self.parameters = [float(x.strip()) for x in weights[0:-1]]


    def output(self, episode, performance):
       print "Performance do episodio #%d: %d" % (episode, performance)
       if episode > 0 and episode % 10 == 0:
           output = open("./params/%s.txt" % datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S'), "w+")
           for parameter in self.parameters:
               output.write(str(parameter) + "\n")

#--------------------------------------------------------------------------------------------------------

    #FUNCAO A SER COMPLETADA. Deve utilizar os pesos para calcular as funções de preferência Q para cada ação e retorna
    #-1 caso a ação desejada seja esquerda, +1 caso seja direita, e 0 caso seja ação nula
    def take_action(self, state):
        self.state.wheel_x = state.wheel_x
        self.state.rod_angle = state.rod_angle
        self.state.angular_velocity = state.angular_velocity
        features = list(self.compute_features())
	#Qe = (self.parameters[0]*features[1])#+((self.parameters[1])-features[2])
	#Q0 = 1
	#Qd = (((-1)*features[1])*(self.parameters[2]-90))#+(self.parameters[1]-features[2])
	Qe = (features[1]-self.parameters[0])*90
	Q0 = 90
	Qd =  (self.parameters[2]-features[1])*90
	preferences = [Qe, Q0, Qd]
        choice = max(preferences)
        #if (features[1] <= self.parameters[0] or features[2] < ((-1)*self.parameters[1])):
        #    return -1
        #elif (features[1] >= self.parameters[2] or features[2] > self.parameters[1]):
        #    return 1
        #else:
        #    return 0
        if (preferences.index(choice) == 0):
            #print "esquerda"
            return -1
        elif (preferences.index(choice) == 2):
            #print "direita"
            return 1
        else:
            return 0


    #FUNCAO A SER COMPLETADA. Deve calcular features expandidas do estados (Dica: deve retornar um vetor)
    def compute_features(self):
        #features = [self.state.wheel_x, 90+self.state.rod_angle, (90-self.state.rod_angle)*self.state.angular_velocity]
        features = [self.state.wheel_x, 90-self.state.rod_angle, (90-self.state.rod_angle)*self.state.angular_velocity]
	#parameters = [0 -> 90, 0 -> 100, 90 -> 180]
        return features

    #FUNCAO A SER COMPLETADA. Deve atualizar a propriedade self.parameters
	#Perfomance is [0,20000]
    def update(self, episode, performance):
        with open('filename.txt', 'a') as f:
            f.write('%d\n' % performance)
        if(episode < MAX_EP):
            cromossome = list(self.parameters)
            cromossome.append(performance)
            self.pop.append(cromossome)
            #print self.pop
            self.parameters = [random.randint(0,90),random.randint(0,100),random.randint(90,180)]
            #self.parameters = numpy.random.normal(0, 1, 3*len(self.compute_features()))
        elif len(self.pop) != 0 and episode%len(self.pop) == 0:
            #print self.pop
            cromossome = list(self.parameters)
            cromossome.append(performance)
            self.pop.append(cromossome)
            populus = genetic.geneticSearch(self.pop)
            #global next_iter
            #next_iter = len(populus)
            #print populus
            self.pop = list(populus)
            #print self.pop
            if self.pop:
                self.parameters = self.pop[0]
            else:
		pass
        else:
            del self.pop[0]
            cromossome = list(self.parameters)
            del cromossome[3]
            cromossome.append(performance)
            self.pop.append(cromossome)
            self.parameters = self.pop[0]
            #print self.pop
        pass
