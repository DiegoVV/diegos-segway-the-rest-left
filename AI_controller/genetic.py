import numpy
from .state import State
import random
import datetime, time
from operator import itemgetter
from itertools import *
debug = 1

class Genetic:
	def geneticSearch(self, pop):

		#cromossome = [random.randint(45,90),random.randint(0,20),random.randint(90,135)]
		#return list(cromossome)

		#Initialize variables:

		#Cromossome = [minimum angle to go to the left, minimum angular velocity to move, minimum angle to go to the right, performance]

		#Sort population based on best evaluatin
		population = self.sortPopulation(pop)
		
		#Cross-over
		population1 = self.crossOver(population)

		return population1

	def sortPopulation(self,population):
		new_pop = list(population)
		#print new_pop
		#sorted(new_pop, key=itemgetter(3), reverse=True) doesn't work for some reason
		new_pop.sort(key=itemgetter(3), reverse=True)
		#	del i[3]
		#print new_pop
		return new_pop

	def evaluation(self,cromossome):
		#Has to execute the program, will return how long it lasted.
		lifeSpam = 0
		cromossome.append(lifeSpam)
		return cromossome

	def crossOver(self,population):
		#Executed after having the population evaluated and sorted.
		new_pop = list(population)
		nextGen = []
		for i in xrange((len(new_pop)/2)-1):
			cromossome1 = []
			cromossome2 = []
			array1 = []
			array2 = []
			section = random.randint(0,len(new_pop[i*2]))
			#print section
			for j in xrange(0,section):
				array1.append(new_pop[i*2][j])
			for j in xrange(section,len(new_pop[(i*2)+1])):
				array2.append(new_pop[(i*2)+1][j])
			cromossome1.extend(array1)
			cromossome1.extend(array2)
			if random.randint(0,10) == 1:
				cromo = self.mutate(cromossome1)
				cromossome1 = cromo
			cromossome2.extend(new_pop[i*2])
			if random.randint(0,10) == 1:
				cromo = self.mutate(cromossome2)
				cromossome2 = cromo
			#if (debug):
				#print "cromossome number ", i
				#print ": ", cromossome
			nextGen.append(cromossome1)
			nextGen.append(cromossome2)
			#print nextGen
		return nextGen

	def mutate(self,cromossome):		
		section = random.randint(0,len(cromossome))
		
		return cromossome

	def generatePopulation(self,size):
		population = []
		for i in range(size):
			cromossome = [random.randint(0,90),random.randint(0,90),random.randint(90,180),random.randint(0,90)]
			population.append(cromossome)

		return population
		
